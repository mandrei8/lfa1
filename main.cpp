#include <iostream>
#include <fstream>
using namespace std;

ifstream f ("lfa1.in");
int nrstari, nrfinale, nrinput, i, finale[10], j, automat[10][10], ok, a, b;
char o;
string cuvant, input[10];

int verificare(string c, int x)
{
    for (int i = 0; i < nrinput; i++)
        if (c == input[i])
            return(automat[i][x]);
    return -1;
}

int main()
{
    //cout<<"Introdu numarul de stari: ";
    f>>nrstari;
    //cout<<"Introdu numarul de stari finale: ";
    f>>nrfinale;
    for (i = 0; i < nrfinale; i++)
    {
        //cout<<"Starea finala numarul "<<i+1<<" este: ";
        f>>finale[i];
    }
    //cout<<"Introdu numarul de elemente din alfabet: ";
    f>>nrinput;
    for (i = 0; i < nrinput; i++)
    {
        //cout<<"Elementul din alfabet cu numarul "<<i+1<<" este: ";
        f>>input[i];
    }
    //cout<<"Tranzitii: "<<endl;
    for (i = 0; i < nrinput; i++)
    {
        for (j = 0; j < nrstari; j++)
        {
            //cout<<"Din starea "<<j<<" cu elementul "<<input[i]<<" ajung in starea: ";
            f>>automat[i][j];
        }
    }
    for (i = 0; i < nrinput; i++)
    {
        for (j = 0; j < nrstari; j++)
            cout<<automat[i][j]<<" ";
        cout<<endl;
    }
    do{
    cout<<"Introdu cuvantul(marcarea sfarsitului se face cu caracterul #): ";
    cin>>cuvant;
    ok = 0;
    a = 0;
    b = 0;
    while (cuvant != "#")
    {
        b = verificare(cuvant, b);
        a = a + 1;
        cin>>cuvant;
        if (b < 0)
            break;

    }
    for (i = 0; i < nrfinale; i++)
    {
        if (b == finale[i])
            ok = 1;
    }
    if (ok == 1)
        cout<<"Cuvantul este acceptat de automat!";
    else
        cout<<"Cuavntul nu este acceptat de automat!";
    cout<<"Continua? (d/n) ";
    cin>>o;
    }while(o == 'd');
}
